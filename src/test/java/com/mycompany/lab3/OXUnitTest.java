package com.mycompany.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class OXUnitTest {
    
    public OXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testCheckWin_O_Horizontal_output_true() { //checkOrow0 
        String table[][] = {{"O", "O", "O"}, {"-", "-", "-"}, {"-", "-", "-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_Horizontal1_output_true() { //checkOrow1
        String table[][] = {{"-", "-", "-"}, {"O", "O", "O"}, {"-", "-", "-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_Horizontal2_output_true() { //checkOrow1
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"O", "O", "O"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_X_Horizontal_output_true() {
        String table[][] = {{"X", "X", "X"}, {"-", "-", "-"}, {"-", "-", "-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_X_Horizontal1_output_true() {
        String table[][] = {{"-", "-", "-"}, {"X", "X", "X"}, {"-", "-", "-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_X_Horizontal2_output_true() {
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"X", "X", "X"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_Vertical_output_true() { 
        String table[][] = {{"O", "-", "-"}, {"O", "-", "-"}, {"O", "-", "-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_Vertical1_output_true() { 
        String table[][] = {{"-", "O", "-"}, {"-", "O", "-"}, {"-", "O", "-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_Vertical2_output_true() { 
        String table[][] = {{"-", "-", "O"}, {"-", "-", "O"}, {"-", "-", "O"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_X_Vertical_output_true() {
        String table[][] = {{"X", "-", "-"}, {"X", "-", "-"}, {"X", "-", "-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_X_Vertical1_output_true() { 
        String table[][] = {{"-", "X", "-"}, {"-", "X", "-"}, {"-", "X", "-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_X_Vertical2_output_true() { 
        String table[][] = {{"-", "-", "X"}, {"-", "-", "X"}, {"-", "-", "X"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_Diag_output_true() { 
        String table[][] = {{"O", "-", "-"}, {"-", "O", "-"}, {"-", "-", "O"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_X_Diag_output_true() { 
        String table[][] = {{"X", "-", "-"}, {"-", "X", "-"}, {"-", "-", "X"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_Diag1_output_true() { 
        String table[][] = {{"-", "-", "O"}, {"-", "O", "-"}, {"O", "-", "-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_X_Diag1_output_true() { 
        String table[][] = {{"-", "-", "X"}, {"-", "X", "-"}, {"X", "-", "-"}};
        boolean result = Lab3.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckDraw_output_true() { 
        String table[][] = {{"X", "O", "O"}, {"O", "X", "X"}, {"-", "X", "O"}};
        boolean result = Lab3.checkDraw(table);
        assertEquals(true, result);
    }


    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
