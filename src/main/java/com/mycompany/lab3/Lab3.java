/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab3;

public class Lab3 {

    
    static boolean checkWin(String [][] table) { 
        if(checkRow(table)) { 
            return true;
        }if(checkCol(table)) {
            return true;
        }if(checkX(table)) {
            return true;
        }if(checkX2(table)) {
            return true;
        }
        return false;
    }
    
    static boolean checkDraw(String [][] table) {
        if(checkWin(table) == true) {
            return false;
        }return true;
    }

    private static boolean checkRow(String[][] table) {
        for(int i=0; i<3; i++) {
            if(table[i][0].equals(table[i][1]) && table[i][1].equals(table[i][2])) {
                return true;
            }
        }return false;
    }
    
    private static boolean checkCol(String[][] table) {
        for(int i=0; i<3; i++) {
            if(table[0][i].equals(table[1][i]) && table[1][i].equals(table[2][i])) {
                return true;
            }
        }return false;
    }
    
    private static boolean checkX(String[][] table) {
        if(table[0][0].equals(table[1][1]) && (table[1][1].equals(table[2][2]))) {
            return true;
        }
        return false;
    }
    
    private static boolean checkX2(String[][] table) {        
        if((table[0][2].equals(table[1][1])) && (table[1][1].equals(table[2][0]))) {
            return true;
        }return false;
    }

    
}
